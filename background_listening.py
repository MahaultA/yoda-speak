import time
import speech_recognition as sr
from textblob import TextBlob
from test import *
from datetime import datetime
import os
import shutil
from yodaspeak import *

NEUTRAL_POLARITY_THRESHOLD = 0.1
 # initialise detector
estimators = get_best_estimators(True)
estimators_str, estimator_dict = get_estimators_name(estimators)
features = ["mfcc", "chroma", "mel"]
emotions = "sad,neutral,happy"
# emotions="neutral,calm,happy,sad,angry,fear,disgust,ps,boredom"
model_name = "BaggingClassifier"
# model_name = "SVC"
detector = EmotionRecognizer(estimator_dict[model_name], emotions=emotions.split(","), features=features, verbose=0)
detector.train()
Yoda = ChatBot(name='Yoda', read_only=True,logic_adapters=['chatterbot.logic.BestMatch'])
small_talk2 = ['hi there!',
                'hi!',
                'how do you do?',
                'how are you?',
                'cool, I am',
                'fine, you?',
                'always cool.',
                'i\'m ok',
                'Glad I am to hear that, ',
                'i\'m fine',
                'glad to hear that.',
                'i feel awesome',
                'excellent, glad to hear that.',
                'not so good',
                'sorry to hear that.',
                'what\'s your name?',
                'baby yoda, I am . Play with me.']

list_trainer = ListTrainer(Yoda)
for item in (small_talk2):
    list_trainer.train(item)
    print(item)

def process_utterance(recognizer, audio):
    try:
        # for testing purposes, we're just using the default API key
        # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        # instead of `r.recognize_google(audio)`
        
        """
        step 1: save audio to a temporary file
        step 2: feed the file to whatever function predicts in test
        
        """

        text = recognizer.recognize_google(audio)
        print(text)
        polarity = TextBlob(text).sentiment.polarity
        subjectivity = TextBlob(text).sentiment.subjectivity
        if polarity >= NEUTRAL_POLARITY_THRESHOLD:
            print("Sentiment Happy", polarity, "subjectivity", subjectivity)
        elif -NEUTRAL_POLARITY_THRESHOLD <= polarity and polarity < NEUTRAL_POLARITY_THRESHOLD:
            print("Sentiment Neutral", polarity, "subjectivity", subjectivity)
        else:
            print("Sentiment Sad", polarity, "subjectivity", subjectivity)

        # sound_file=audio.save()
        # detector.predict(sound_file)

        # write audio to a WAV file
        file_name =f"tmp/{datetime.now().timestamp()}.wav"
        with open(file_name, "wb") as f:
            f.write(audio.get_wav_data())
        prosody = detector.predict(file_name)
        print("Prosody emotion", prosody)
        
        
        
        val = text
        print("Baby Yoda says:", Yoda.get_response(val))

    except sr.UnknownValueError:
        #print("Google Speech Recognition could not understand audio")
        print("...")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))

def main():
    
    try:
        os.mkdir("tmp")
    except FileExistsError:
        pass

    recognizer = sr.Recognizer()
    microphone = sr.Microphone()
    print("calibrating...")
    with microphone as source:
        recognizer.adjust_for_ambient_noise(source)  # we only need to calibrate once, before we start listening

    print("listening...")
    # start listening in the background (note that we don't have to do this inside a `with` statement)
    stop_listening = recognizer.listen_in_background(microphone, process_utterance)
    # `stop_listening` is now a function that, when called, stops background listening

    try:
        while True:
            time.sleep(5)
            #print("still awake...")
    except:
        pass
    finally:
        print("listening stopped")
        stop_listening()
        shutil.rmtree("tmp")


if __name__=="__main__":
    main()


