from chatterbot import ChatBot 
from chatterbot.trainers import ListTrainer 


my_bot = ChatBot(name='Yoda', read_only=True,logic_adapters=['chatterbot.logic.BestMatch'])
                                 
def main():
    small_talk = ['hi there!',
                'hi!',
                'how do you do?',
                'how are you?',
                'cool, I am',
                'fine, you?',
                'always cool.',
                'i\'m ok',
                'Glad I am to hear that, ',
                'i\'m fine',
                'glad to hear that.',
                'i feel awesome',
                'excellent, glad to hear that.',
                'not so good',
                'sorry to hear that.',
                'what\'s your name?',
                'baby yoda, I am . Play with me.']
    math_talk_1 = ['pythagorean theorem',
                'a squared plus b squared equals c squared.']
    math_talk_2 = ['law of cosines',
                'c**2 = a**2 + b**2 - 2 * a * b * cos(gamma)']

    list_trainer = ListTrainer(my_bot)
    for item in (small_talk, math_talk_1, math_talk_2):
        list_trainer.train(item)

    val = input("say something: ")
    print(my_bot.get_response(val))

if __name__ == "__main__":
   main()