import time
import speech_recognition as sr
from textblob import TextBlob
import test
from test import *
from test import main
import tempfile
from tempfile import NamedTemporaryFile
import io
from pydub import AudioSegment

NEUTRAL_POLARITY_THRESHOLD = 0.1
 # initialise detector
estimators = get_best_estimators(True)
estimators_str, estimator_dict = get_estimators_name(estimators)
features = ["mfcc", "chroma", "mel"]
detector = EmotionRecognizer(estimator_dict["BaggingClassifier"], emotions="sad,neutral,happy".split(","), features=features, verbose=0)
detector.train()


def process_utterance(recognizer, audio):
    try:
        # for testing purposes, we're just using the default API key
        # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        # instead of `r.recognize_google(audio)`
        
        """
        step 1: save audio to a temporary file
        step 2: feed the file to whatever function predicts in test
        
        """

        text = recognizer.recognize_google(audio)
        print(text)
        polarity = TextBlob(text).sentiment.polarity
        subjectivity = TextBlob(text).sentiment.subjectivity
        if polarity >= NEUTRAL_POLARITY_THRESHOLD:
            print("Happy", polarity, "subjectivity", subjectivity)
        elif -NEUTRAL_POLARITY_THRESHOLD <= polarity and polarity < NEUTRAL_POLARITY_THRESHOLD:
            print("Neutral", polarity, "subjectivity", subjectivity)
        else:
            print("Sad", polarity, "subjectivity", subjectivity)
        ##test.main()
        #sound_file=audio.save()
        #sound_file="microphone-results.wav"
        print(type(audio))
        #detector.predict(sound_file)
        #detector.predict("microphone-results.wav")
        s = io.BytesIO(current_data)
        newaudio = AudioSegment.from_raw(s, sample_width, frame_rate, channels).export(filename, format='wav')
        # write audio to a WAV file
       # with open("microphone-results.wav", "wb") as f:
            
            # When delete=False is specified, this file will not be
            # removed from disk automatically upon close/garbage collection
            #f = NamedTemporaryFile(delete=False)
            #f=tempfile.NamedTemporaryFile(mode='w+t',
            #suffix='.wav', delete=False)
            #f=tempfile.mkstemp(suffix='.wav')
            # Save the file path
            #path = f.name
            #print(f)
            # Write something to it
            #f[1].write(audio.get_wav_data())
            #detector.predict(f)
            #detector.predict(audio)
        # with open("microphone-results.wav", "wb") as f:
        #     f.write(audio.get_wav_data())
        #     record_to_file(f)
        #     print(type(f))
        #     #detector.predict(f)

    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))

def main():
   

    recognizer = sr.Recognizer()
    microphone = sr.Microphone()
    print("calibrating...")
    with microphone as source:
        recognizer.adjust_for_ambient_noise(source)  # we only need to calibrate once, before we start listening

    print("listening...")
    # start listening in the background (note that we don't have to do this inside a `with` statement)
    stop_listening = recognizer.listen_in_background(microphone, process_utterance)
    # `stop_listening` is now a function that, when called, stops background listening

    try:
        while True:
            time.sleep(5)
            print("still awake...")
    except:
        pass
    finally:
        print("listening stopped")
        stop_listening()

if __name__=="__main__":
    main()


